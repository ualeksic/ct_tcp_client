﻿/*
   Copyright 2011 CrypTool 2 Team <ct2contact@cryptool.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
using System.ComponentModel;
using System.Windows.Controls;
using Cryptool.PluginBase;
using Cryptool.PluginBase.Miscellaneous;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System;

namespace Cryptool.Plugins.Public_TCP_Client
{
    // HOWTO: Change author name, email address, organization and URL.
    [Author("Univerzitet Singidunum", "coredevs@cryptool.org", "Crypto Singidunum", "http://cryptool2.vs.uni-due.de")]
    // HOWTO: Change plugin caption (title to appear in CT2) and tooltip.
    // You can (and should) provide a user documentation as XML file and an own icon.
    [PluginInfo("TCP Client", "TCP Communitaion", "Public_TCP_Client/userdoc.xml", new[] { "Public TCP Client/icon.png" })]
    // HOWTO: Change category to one that fits to your plugin. Multiple categories are allowed.
    [ComponentCategory(ComponentCategory.Protocols)]
    public class Public_TCP_Client : ICrypComponent
    {
        #region Private Variables

        // HOWTO: You need to adapt the settings class as well, see the corresponding file.
        private readonly ExamplePluginCT2Settings settings = new ExamplePluginCT2Settings();
        private static TcpClient nodeClient;
        private static NetworkStream nodeStream;
        #endregion

        #region TCP Client Implementation

        void initializeClient() 
        {
            try
            {
                nodeClient = new TcpClient();
                nodeClient.Connect(IPAddress.Parse(settings.ServerIp), settings.ServerPort);
            }
            catch (Exception e)
            {
                GuiLogMessage(e.ToString(), NotificationLevel.Error);
                GuiLogMessage("Server unreachable.", NotificationLevel.Error);
            }
        }

        void HandleData() {
            nodeStream = nodeClient.GetStream();
            nodeStream.Write(msgByte, 0, msgByte.Length);
            outByte = msgByte;
            OnPropertyChanged("outByte");
            nodeStream.Flush();
            byte[] responseBuffer = new byte[(int)nodeClient.ReceiveBufferSize];
            nodeStream.Read(responseBuffer, 0, (int)nodeClient.ReceiveBufferSize);
            responseByte = Encoding.ASCII.GetBytes(Encoding.ASCII.GetString(responseBuffer).Replace("\0", String.Empty));
            OnPropertyChanged("responseByte");
            nodeClient.Close();

        }

        #endregion


        #region Data Properties

        /// <summary>
        /// HOWTO: Input interface to read the input data. 
        /// You can add more input properties of other type if needed.
        /// </summary>
        [PropertyInfo(Direction.InputData, "Message", "Binary data for sending over network")]
        public byte[] msgByte
        {
            get;
            set;
        }

        /// <summary>
        /// HOWTO: Output interface to write the output data.
        /// You can add more output properties ot other type if needed.
        /// </summary>
        [PropertyInfo(Direction.OutputData, "Message", "Sent Message.")]
        public byte[] outByte
        {
            get;
            set;
        }


        [PropertyInfo(Direction.OutputData, "Response Message", "Response Message")]
        public byte[] responseByte
        {
            get;
            set;
        }

        #endregion

        #region IPlugin Members

        /// <summary>
        /// Provide plugin-related parameters (per instance) or return null.
        /// </summary>
        public ISettings Settings
        {
            get { return settings; }
        }

        /// <summary>
        /// Provide custom presentation to visualize the execution or return null.
        /// </summary>
        public UserControl Presentation
        {
            get { return null; }
        }

        /// <summary>
        /// Called once when workflow execution starts.
        /// </summary>
        public void PreExecution()
        {
        }

        /// <summary>
        /// Called every time this plugin is run in the workflow execution.
        /// </summary>
        public void Execute()
        {
             // HOWTO: Use this to show the progress of a plugin algorithm execution in the editor.
            ProgressChanged(0, 1);
            this.initializeClient();
            this.HandleData();
            ProgressChanged(1, 1);
        }

        /// <summary>
        /// Called once after workflow execution has stopped.
        /// </summary>
        public void PostExecution()
        {
        }

        /// <summary>
        /// Triggered time when user clicks stop button.
        /// Shall abort long-running execution.
        /// </summary>
        public void Stop()
        {
        }

        /// <summary>
        /// Called once when plugin is loaded into editor workspace.
        /// </summary>
        public void Initialize()
        {
        }

        /// <summary>
        /// Called once when plugin is removed from editor workspace.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion

        #region Event Handling

        public event StatusChangedEventHandler OnPluginStatusChanged;

        public event GuiLogNotificationEventHandler OnGuiLogNotificationOccured;

        public event PluginProgressChangedEventHandler OnPluginProgressChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        private void GuiLogMessage(string message, NotificationLevel logLevel)
        {
            EventsHelper.GuiLogMessage(OnGuiLogNotificationOccured, this, new GuiLogEventArgs(message, this, logLevel));
        }

        private void OnPropertyChanged(string name)
        {
            EventsHelper.PropertyChanged(PropertyChanged, this, new PropertyChangedEventArgs(name));
        }

        private void ProgressChanged(double value, double max)
        {
            EventsHelper.ProgressChanged(OnPluginProgressChanged, this, new PluginProgressEventArgs(value, max));
        }

        #endregion
    }
}
